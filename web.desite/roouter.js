angular.module('application', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: 'home',
                templateUrl: 'listContent.html',
            })
    });