'use strict';
var app = angular.module('ListContent', []);
app.controller('storeController', ['$scope', 'dataService', '$http', function ($scope, dataSvc, $http) {
    http({
        url: 'http://localhost:3000/listContent',
        method: 'get'
    }).
    then(function (response) {
        console.log(response.data);
        $scope.games = response.data;
    });
}]);