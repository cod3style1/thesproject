'use strict';

var mainpageContent = 'mainPage';
var mainAppModule = angular.module('mainpageContent',['ui.router','mainpage']);

angular.element(document).ready(function(){
    angular.bootstrap(document.querySelector('#mainPage'),['mainpageContent'],{
        strictDi: true
    });
});


