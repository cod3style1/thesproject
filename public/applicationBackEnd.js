'use strict';

var backendContent = 'backendPage';
var mainAppModule = angular.module('backendContent',['ui.router','backend']);

angular.element(document).ready(function(){
    angular.bootstrap(document.querySelector('#backEnd'),['backendContent'],{
        strictDi: true
    });
});