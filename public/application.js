'use strict';

var mainAppModuleName ='Hello';
var mainAppModule = angular.module('mainAppModuleName',['ui.router','core','users']);
angular.element(document).ready(function(){
    angular.bootstrap(document.querySelector('#mainApp'),['mainAppModuleName'],{
        strictDi: true
    });
});

mainAppModule.controller('NameController', ['$scope','$http', function($scope, $http){
    $scope.youName = 'no Name';
    var user_json = $http.get('/user');
}]);

mainAppModule.filter('sayhello', function(){
    return function(name){
        return 'Hello,' + name;
    };
});