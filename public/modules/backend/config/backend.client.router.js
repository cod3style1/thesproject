angular.module('backend').config([
    '$stateProvider',
    function($stateProvider){
        $stateProvider
            .state('about',{
                url: '/about',
                templateUrl: '/modules/backend/views/aboutBack.client.view.jade'
            })
            .state('service',{
                url: '/service',
                templateUrl: '/modules/backend/views/serviceBack.client.view.jade'
            }) 
            .state('personnel',{
                url: '/personnel',
                templateUrl: '/modules/backend/views/personnelBack.client.view.jade'
            })
            .state('partners',{
                url: '/partners',
                templateUrl: '/modules/backend/views/partnerBack.client.view.jade'
            })
            .state('contactus',{
                url: '/contactus',
                templateUrl: '/modules/backend/views/contactBack.client.view.jade'
            })
            .state('slide',{
                url: '/slide',
                templateUrl: '/modules/backend/views/slideBack.client.view.jade'
            })
            .state('work',{
                url: '/work',
                templateUrl: '/modules/backend/views/workBack.client.view.jade'
            });
}]);

