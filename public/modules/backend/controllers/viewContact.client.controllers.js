angular.module('backend').controller('viewContactController',
    ['$scope', '$http', function( $scope, http ){
        
        loadData();
        var model;
        $scope.model=[{}];
        
        //insert 
        $scope.addData =function(){
            http({
                url: 'http://localhost:3000/Contact/', 
                method: 'post',
                data: this.model
            }).
            then(function (response){
                loadData();
            });
           $scope.model={};
        }

        //view
        function loadData(){
            http({
                url: 'http://localhost:3000/Contact/',
                method: 'get',
            }).
            then(function (response){
                $scope.showList = true;
                $scope.contacts = response.data;
            });
            $scope.model={};
        }

        //delete
        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Contact/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        //button edit
        $scope.editData = function(id){ 
            console.log(id)
            http({
                url: 'http://localhost:3000/Contact/' + id,
                method: 'get'
            }).
            then(function (response){
                $scope.showList = false;
                $scope.contacts = response.data;
               $scope.model =  $scope.contacts;
            });
        }
        //update
        $scope.updateData = function(){
            console.log($scope.contacts._id);
            http.put('http://localhost:3000/Contact/' + $scope.contacts._id, $scope.model).success(function(response){
                loadData();
            })
        }
        
        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
}]);