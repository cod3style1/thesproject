angular.module('backend').controller('slideController',
    ['$scope','$http', function($scope, http){

        loadData();
        
        function loadData(){
            http({
                url: 'http://localhost:3000/Slide/',
                method: 'get',
            }).
            then(function (response){
                $scope.showList = true;
                $scope.slides = response.data;
            });
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Slide/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
        
    }]
)