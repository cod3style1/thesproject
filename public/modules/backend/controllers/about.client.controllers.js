angular.module('backend').controller('aboutController',
    ['$scope','$http', function($scope, http){
        
        loadData();
        var model;
        $scope.model=[{}];
    
        $scope.editData = function(id){  
            console.log(id)
            http({
                url: 'http://localhost:3000/About/' + id,
                method: 'get'
            }).
            then(function (response){
                $scope.showList = false;
                $scope.abouts = response.data;
                $scope.model =  $scope.abouts;
            });
        }
 
        $scope.updateData = function(){
            console.log($scope.abouts._id);
            http.put('http://localhost:3000/About/' + $scope.abouts._id, $scope.model).success(function(response){
                loadData();
            })
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/About/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        function loadData(){
            http({
                url: 'http://localhost:3000/About/',
                medthod: 'get'
            }).
            then(function(response){
                $scope.showList = true;
                $scope.abouts = response.data;
            });
        }

        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
    }]
);