angular.module('backend').controller('workController',
    ['$scope', '$http', function($scope, http){
        loadData(); 
        var model;
        $scope.model=[{}];
        
        $scope.addData =function(){
            http({
                url: 'http://localhost:3000/Work/', 
                method: 'post',
                data: this.model
            }).
            then(function (response){
                loadData();
            });
           $scope.model={};
        }

        function loadData(){
            http({
                url: 'http://localhost:3000/Work/',
                method: 'get',
            }).
            then(function (response){
                $scope.works = response.data;
            });
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Work/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        $scope.editData = function(id){ 
            console.log(id)
            http({
                url: 'http://localhost:3000/Work/' + id,
                method: 'get'
            }).
            then(function (response){
                $scope.showList = false;
                $scope.works = response.data;
                $scope.model =  $scope.works;
            });
        }

        $scope.updateData = function(){
            console.log($scope.works._id);
            http.put('http://localhost:3000/Work/' + $scope.works._id, $scope.model).success(function(response){
                loadData();
            })
        }

        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
    }]
)