angular.module('backend').controller('serviceController',
    ['$scope', '$http', function($scope, http){

        var model;
        loadData();
        $scope.model=[{}];
        $scope.showList = true;

        $scope.addData =function(){
            http({
                url: 'http://localhost:3000/Service/', 
                method: 'post',
                data: this.model
            }).
            then(function (response){  
                loadData();
            });
           $scope.model={};
        }

        $scope.editData = function(id){ 
            console.log(id)
            http({
                url: 'http://localhost:3000/Service/' + id,
                method: 'get'
            }).
            then(function (response){
                $scope.showList = false;
                $scope.services = response.data;
                $scope.model =  $scope.services;
            });
        }

        $scope.updateData = function(){
            console.log($scope.services._id);
            http.put('http://localhost:3000/Service/' + $scope.services._id, $scope.model).success(function(response){
                loadData();
            })
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Service/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        function loadData(){
            http({
                url: 'http://localhost:3000/Service/',
                medthod: 'get'
            }).
            then(function(response){
                $scope.showList = true;
                $scope.services = response.data;
            });
        }

        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }

    }]
)