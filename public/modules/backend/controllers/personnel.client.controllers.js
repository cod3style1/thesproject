angular.module('backend').controller('personnelController',
    ['$scope','$http', function($scope, http){
        
        loadData();
        var model;
        $scope.model=[{}];

        $scope.addData =function(){
            //var path = req.files.image.path;
            http({
                url: 'http://localhost:3000/Personnel/', 
                method: 'post',
                data: this.model
            }).
            then(function (response){ 
                
                loadData();
            });
           $scope.model={};
        }

        function loadData(){
            http({
                url: 'http://localhost:3000/Personnel/',
                method: 'get',
            }).
            then(function (response){
                $scope.showList = true;
                $scope.personnels = response.data;
            });
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Personnel/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
    }]
)