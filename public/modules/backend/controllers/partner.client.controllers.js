angular.module('backend').controller('partnerController',
    ['$scope', '$http', function($scope, http){

        loadData();
        var model;
        $scope.model=[{}];
        
        $scope.addData =function(){
            console.log(files);
            //var path = req.files.image.path;
            http({
                url: 'http://localhost:3000/Partner/', 
                method: 'post',
                data: this.model
            }).
            then(function (response){
                
                loadData();
            });
           $scope.model={};
        }

        function loadData(){
            http({
                url: 'http://localhost:3000/Partner/',
                method: 'get',
            }).
            then(function (response){
                $scope.showList = true;
                $scope.partners = response.data;
            });
            $scope.model={};
        }

        $scope.deleteData = function(id){
            http({
                url: 'http://localhost:3000/Partner/' + id,
                method: 'delete'
            }).
            then(function (response){
                loadData();
            });
        }

        $scope.editData = function(id){ 
            console.log('click edit',id);
            http({
                url: 'http://localhost:3000/Partner/' + id,
                method: 'get'
            }).
            then(function (response){
                $scope.showList = false;
                $scope.partners = response.data; 
               $scope.model =  $scope.partners;
            });
        }
        
        $scope.updateData = function(){
            console.log($scope.partners._id);
            http.put('http://localhost:3000/Partner/58275f84b02c03902389904e' , $scope.model).success(function(response){
                loadData();
            })
        }
        
        $scope.showInsert = function(){
           $scope.showList = false;
        }

        $scope.showList = function(){
           $scope.showList = true;
        }
    }]
)