angular.module('mainpage').controller('slideViewController',
    ['$scope', '$http', function($scope, http){
        loadData();
        function loadData(){
            http({
                url: 'http://localhost:3000/Slide' ,
            }).
            then(function (response){
                $scope.fistSlide = response.data[0];
                $scope.slides = response.data.slice(1, response.data.length);
            });
        }
    }]
)