angular.module('mainpage').controller('personnelViewController',
    ['$scope','$http', function($scope, http){
        loadData();
        function loadData(){
            http({
                url: 'http://localhost:3000/Personnel/',
                method: 'get'
            }).
            then(function (response){
                //cut fist array
                $scope.firstpersonal = response.data[0];
                $scope.personnels = response.data.slice(1, response.data.length);
            });
        }
    }]
) 