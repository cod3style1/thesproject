angular.module('mainpage').controller('viewController',
    ['$scope','$http', function($scope, http){
        viewService();
        viewAbout();
        viewContact();
        viewWork();
        function viewAbout(){
            http({
                url: 'http://localhost:3000/About/',
                method: 'get'
            }).
            then(function (response){
                $scope.abouts = response.data;
            });
        }
        function viewService(){
            http({
                url: 'http://localhost:3000/Service/',
                method: 'get'
            }). 
            then(function(response){
                $scope.services = response.data;
            });
        }
        function viewContact(){
            http({
                url: 'http://localhost:3000/Contact/',
                method: 'get'
            }).
            then(function(response){
                console.log(response.data);
                $scope.contacts = response.data;
            });
        }
        function viewWork(){
            http({
                url: 'http://localhost:3000/Work/',
                method: 'get'
            }).
            then(function(response){
                console.log(response.data);
                $scope.works = response.data;
            });
        }
    }]
);