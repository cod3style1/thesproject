var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var WorkSchema = new Schema({
    topic: String,
    content: String,
    name: String,
    path: String
});
mongoose.model('work', WorkSchema);