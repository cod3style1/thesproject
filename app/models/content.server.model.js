var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ContentSchema = new Schema({
    titleContent: String,
    textContent: String
});
mongoose.model('content', ContentSchema);  