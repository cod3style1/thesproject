var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SlideSchema = new Schema({
    name: String,
    path: String
});
mongoose.model('slide', SlideSchema);