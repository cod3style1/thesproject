var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ServiceSchema = new Schema({
    contentOne: String,
    contentTwo: String,
    name: String,
    path: String
});
mongoose.model('service', ServiceSchema);