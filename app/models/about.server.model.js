 var mongoose = require('mongoose');
 var Schema = mongoose.Schema;
 var AboutSchema = new Schema({
     history: String,
     vision: String,
     mission: String 
 });
 mongoose.model('about', AboutSchema);