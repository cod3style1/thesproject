var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var UserSchema = new Schema({
    fistName: String,
    lastName: String,
    username: {
        type: String,
        unique: true,
        required: 'Username is require',
        //กันเแว้นวรรค
        trim: true,
        //ทำให้จำเป็นต้องใส่ username
        require: true
    },
    email: {
        type: String,
        index: true,
        //ตรวจสอบว่าเป็น email รึป่าว
        match: /.+\@.+\.+/
    },
    password: {
        type: String,
        validate:[
            function(password){
                return password && password.length >= 6;
            },
            'password must be at least 6 characters'
        ]
    },
    //กันการแก้รหัสผ่าน โดยใช้ salt คู่กับ planeText เพื่อเข้ารหัส
    salt: {
        type: String
    },
    //ตรวจเข้าระบบแบบไหน 
    provider:{
        type: String,
        required: 'Provider is required'
    },
    providerId: String,
    providerData:{},

    //บอกค่าเวลาสร้าง
    created: {
        type : Date,
        default: Date.now
    }
});

//การนำ password มาเข้ารหัส
UserSchema.pre('save', function(next){
    if(this.password){
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});

UserSchema.methods.hashPassword = function(password){
    return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

UserSchema.methods.authenticate = function(password){
    return this.password === this.hashPassword(password);
};
UserSchema.statics.findUniqueUsername = function(username, suffix, callback){
    var _this = this;
    var possibleUsername = username + (suffix || '');
    _this.findOne({
        username: possibleUsername
    }, function(err, user){
        if(!err){
            if(!user) callback(possibleUsername);
            else return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
        }else{
            callback(null);
        }
    });
};

mongoose.model('User', UserSchema);





/*
UserSchema.statics.findUniqueUsername = function(username, suffix, callback){
    var _this = this;
    var possibleUsername = username + (suffix || '');
    _this.findOne({
        username: possibleUsername
    }, function(err, user){
        if(!err){
            if(!user) callback(possibleUsername);
            else return _this.findUniqueUsername(username,(suffix || 0) + 1 , callback);
        }else{
            callback(null);
        }
    });
};*/
