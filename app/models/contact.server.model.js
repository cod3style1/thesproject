var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ContactSchema = new Schema({
    company: String,
    address: String,
    email: String
});
mongoose.model('contact', ContactSchema);