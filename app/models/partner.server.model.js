var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PartnerSchema = new Schema({
    name: String,
    path: String
});
mongoose.model('partner', PartnerSchema);