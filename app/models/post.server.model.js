//การ
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    content: {
        type: String,
        require: true
    },
    author:{
        type: Schema.ObjectId,
        //การลิ้ง
        ref: 'User'
    }
});

/* 
// การเรียกใช้

var user = new User();
user.save();

var post = new post();
post.auther = user;
post.save();

 */