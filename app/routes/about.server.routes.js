var controller_about = require('../controllers/about.server.controller');
var express = require('express');

module.exports = function(app){
    app.route('/About')
        .get(controller_about.viewAbout)
    app.route('/About/:_id')
        .get(controller_about.findOneAbout)
        .put(controller_about.findOneUpdateAbout);
    app.param('_id', controller_about.aboutById);
};