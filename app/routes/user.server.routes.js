var user = require('../controllers/user.server.controller');
var passport = require('passport');
module.exports = function(app){
    app.route('/signup')
        .get(user.renderSignup)
        .post(user.signup);
    app.route('/login')
        .get(user.renderLogin)
        .post(passport.authenticate('local',{
            successRedirect: '/admin',
            failureRedirect: '/admin',
            failureFlash: true
        }));
    app.post('/logout', user.logout);

    app.get('/oauth/google', passport.authenticate('google',{
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ],
        failureRedirect: '/login'
    }));
    app.get('/oauth/google/callback', passport.authenticate('google',{
        failureRedirect: '/login',
        successRedirect: '/test'
    }));

    app.get('/oauth/facebook', passport.authenticate('facebook',{
        failureRedirect: '/login',
        scope: 'email'
    }));
    app.get('/oauth/facebook/callback', passport.authenticate('facebook',{
        failureRedirect: '/login',
        successRedirect: '/test'
    })); 
    app.route('/user')
        .post(user.create)
        .get(user.list);

    //url param
    app.route('/user/:_id')
        .get(user.read)
        .put(user.update)
        .delete(user.delete);
    app.param('_id',user.userByUsername);
};