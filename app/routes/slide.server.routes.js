var controller_slide = require('../controllers/slide.server.controller');
var express =require('express');
module.exports = function(app){
    app.route('/Slide')
        .get(controller_slide.viewSlide)
        .post(controller_slide.insertSlide);
    app.route('/Slide/:_id')
        .delete(controller_slide.deleteSlide);
    app.param('_id', controller_slide.slideById);
};