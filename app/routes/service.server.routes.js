var controller_service = require('../controllers/service.server.controller');
var express = require('express');

module.exports = function(app){
    app.route('/Service')
        .get(controller_service.viewService)
        .post(controller_service.insertService);
    app.route('/Service/:_id')
        .get(controller_service.findOneService)
        .put(controller_service.findOneUpdateService)
        .delete(controller_service.deleteService);
    app.param('_id', controller_service.serviceById);
}