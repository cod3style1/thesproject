var controller_content = require('../controllers/content.server.controller');
var express = require('express');

var router = express.Router();
module.exports = function(app){
    app.get('/',controller_content.Test);
    app.get('/admin',controller_content.renderBackEnd);
    app.route('/insertcontent')
        .post(controller_content.newContent)
        .get(controller_content.renderCreate);
    app.get('/viewcontents',controller_content.renderContents);
    app.route('/listContent')
        .get(controller_content.listContent);
    app.route('/listContent/:_id')
        .put(controller_content.update)
        .delete(controller_content.delete);
}; 