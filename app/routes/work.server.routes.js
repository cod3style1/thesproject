var controller_work = require('../controllers/work.server.controller');
var express = require('express');

module.exports = function(app){
    app.route('/Work')
        .get(controller_work.viewWork)
        .post(controller_work.insertWork);
    app.route('/Work/:_id')
        .get(controller_work.findOneWork)
        .put(controller_work.findOneUpdateWork)
        .delete(controller_work.findOneDeleteWork);
    app.param('_id',controller_work.workById);
};