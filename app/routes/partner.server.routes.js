var controller_partner = require('../controllers/partner.server.controller');
var express = require('express');
var multer = require('multer');
module.exports = function(app){
    app.route('/Partner')
        .get(controller_partner.viewUpload) 
        .post(controller_partner.uploadFile);
    app.route('/Partner/:_id')
        .get(controller_partner.findOnePartner)
        .put(controller_partner.updatePartner)
        .delete(controller_partner.deletePartner);
    app.param('_id', controller_partner.partnerById);
} 