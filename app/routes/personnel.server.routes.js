var controller_personnel = require('../controllers/personnel.server.controller');
var express = require('express');
module.exports = function(app){
    app.route('/Personnel')
        .get(controller_personnel.viewPersonnel)
        .post(controller_personnel.insertPersonnel);
    app.route('/Personnel/:_id')
        .delete(controller_personnel.deletePersonnel);
    app.param('_id', controller_personnel.personnelById);
};