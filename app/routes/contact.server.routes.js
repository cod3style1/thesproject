var controller_contact = require('../controllers/contact.server.controller');
var express = require('express');

module.exports = function(app){
    app.route('/Contact')
        .get(controller_contact.viewContact)
        .post(controller_contact.insertContact);
    //action by id
    app.route('/Contact/:_id')
        .get(controller_contact.contactFindOne)
        .put(controller_contact.contactUpdate)
        .delete(controller_contact.contactDelete);
    app.param('_id',controller_contact.contactById);
};