/*
exports.login = function(req, res){
    console.log(req.body);
    console.log('Email: ' +req.body.email);
    console.log('Password: ' +req.body.password);
    req.checkBody('email','Invalid email').notEmpty().isEmail();
    req.sanitizeBody('email').normalizeEmail();
    var errors = req.validationErrors();
//การแสดงข้อผิดพลาด เมื่อไม่ใส่ email ที่ถูกต้อง
    if(errors){
        res.render('index',{
            title: 'There have been validation errors: ' + JSON.stringify(errors),
            isLoggedIn: false
        });
        return;
    }
// จำ user password
    if(req.body.remember === 'remember'){
        req.session.remember = true;
        req.session.email = req.body.email;
    }

//รวมไฟล์ viwe 
    res.render('index',{
        title: 'Logged in as ' +req.body.email,
        isLoggedIn: true
    });
};
*/
exports.renderLogin = function(req, res){
    if(!req.user){
        res.render('login',{
            title: 'log in',
            messages: req.flash('error') || req.flash('info')
        });
    }else{
        return res.redirect('/admin');
    }
};


//กดมาหน้า signup
exports.renderSignup = function(req, res){
    if(!req.user){
        res.render('signup',{
            title: 'Sign up',
            messages: req.flash('error')
        });
    }else{
        return res.redirect('/admin');
    }
};

//การส่งข้อมูลหน้า signup
exports.signup = function(req, res, next){
    if(!req.user){
        var user = new User(req.body);
        user.provider = 'local';

        user.save(function(err){
            if(err){
                var message = getErrorMessage(err);
                req.flash('error',message);
                return res.redirect('/signup');
            }
            req.login(user, function(err){
                if(err) return next(err);
                return res.redirect('/admin');
            });
        });
    }else{
        return res.redirect('/admin');
    }
};

//การออกระบบ 
exports.logout = function(req, res){
    req.logout();
    res.redirect('/');
};

//การสร้าง model .create 
var User = require('mongoose').model('User');
exports.create = function(req,res, next) {
    var user = new User(req.body);
    user.save(function(err){
        if(err){
            return next(err);
        }else{
            res.json(user);
        }
    });
};

var getErrorMessage = function(err){
    var message='';
    if(err.code){
        switch (err.code){
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
            default:
                message = 'Something went wrong';
        }
    }else{
        for (var errName in err.errors){
            if(err.errors[errName].message){
                message = err.errors[errName].message;
            }
        }
    }
    return message;
};

//การอ่านข้อมูล .find()
exports.list = function(req,res ,next){
    User.find({}, function(err, users){
        if(err){
            return next(err);
        }else{
            res.json(users);
        }
    });
};

exports.read = function(req, res){
    res.json(req.user);
};

exports.update = function(req, res , next){
    User.findOneAndUpdate({username: req.user.username},req.body,
        function(err, user){
            if(err){
                return next(err); 
            }else{
                res.json(user);
            }
        });
};

exports.delete = function(req, res , next){
    req.user.remove(function(err){
        if(err){
            return next(err);
        }else{
            res.json(user);
        }
    });
};

exports.userByUsername = function(req, res, next, _id){
    User.findOne({
        _id : _id
    }, function(err , user){
        if(err){
            return next(err);
        }else{
            req.user = user;
            next();
        }
    });
};

exports.saveOAuthUserProfile = function(req, profile, done){
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function(err, user){
        if(err) return done(err);
        else{
            if(!user){
                var possibleUsername = profile.username || (profile.email ? profile.email.split('@')[0]:'');
                User.findUniqueUsername(possibleUsername, null, function(availableUsername){
                    profile.username = availableUsername;
                    user = new User(profile);
                    user.save(function(err){
                        if(err){
                            var message = getErrorMessage(err);
                            req.flash('error', message);
                            return res.redirect('/signup');
                        }
                        return done(err, user);
                    });
                });
            }else{
                return done(err, user);
            }
        }
    });
};

/*
exports.saveOAuthUserProfile = function(req, profile, done){
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function(err, user){
        if(err) return done(err);
        else{
            if(!user){
                var possibleUsername = profile.username || (profile.email ? profile.email.split('@')[0]:'');
                User.findUniqueUsername(possibleUsername, null, function(availableUsername){
                    profile.username = availableUsername;
                    user = new User(profile);
                    user.save(function(err){
                        if(err){
                            var message = getErrorMessage(err);
                            req.flash('error', message);
                            return res.redirect('/signup');
                        }
                        return done(err, user);
                    });
                });
            }else{
                return done(err, user);
            }
        }
    }
    );
};*/