var Contact = require('mongoose').model('contact');
//view all json
exports.viewContact = function(req, res, next){
   // res.render('upload');

    Contact.find({}, function(err, contact){
        if(err){
            return next(err);
        }else{
            res.json(contact);
        }
    });
};

//insert
exports.insertContact = function(req, res, next){
    var NewContact = new Contact();
    NewContact.company = req.body.company;
    NewContact.address = req.body.address;
    NewContact.email = req.body.email;

    NewContact.save(function(err, contact){
        if(err){
            res.send('error saving contact');
        }else{
            console.log(contact);
            res.send(contact);
        }
    });
}

//findOne
exports.contactFindOne = function(req, res){
    res.json(req.contact);
};  

/*
exports.contactUpdate = function(req, res, next){
    var id = req.params.id;
    
    console.log(req.body.company);
    db.contacts.findAndModify({query: {_id: mongojs.ObjectId(id)},
        update: {$set:{company: req.body.company, address: req.body.address, email: req.body.email}},
        new: true}, function(err, contact){
            res.json(contact);
        }
    );
};*/

//updateByid
exports.contactUpdate = function(req, res, next){
    Contact.findOneAndUpdate({company: req.contact.company},req.body,
        function(err, contact){
            if(err){
                return next(err);
            }else{
                res.json(contact);
            }
        } 
    );
};

//deletaByid
exports.contactDelete = function(req, res,  next){
    req.contact.remove(function(err){
        if(err){
            return next(err);
        }else{
            res.json('remove success!');
        }
    });
};

//params find id
exports.contactById = function(req, res, next, _id){
    Contact.findOne({
        _id : _id
    }, function(err , contact){
        if(err){
            return next(err);
        }else{
            req.contact = contact;
            next();
        }
    });
};
