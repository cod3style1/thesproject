var Service = require('mongoose').model('service');
var fs = require('fs');
//view all json
exports.viewService = function(req, res, next){
    Service.find({}, function(err, service){
        if(err){
            return next(err);
        }else{
            res.json(service);
        }
    });
};

//insert service
exports.insertService = function(req, res, next){
    var path = req.files.image.path;
    var newService = new Service();
    newService.contentOne = req.body.contentOne;
    newService.contentTwo = req.body.contentTwo;
    newService.name = req.files.image.name;
    newService.path = path;
    newService.save(function(err, service){
        if(err){
            res.send('cannot insert data');
        }else{
            return res.redirect('/admin');
        }
    });
};

//find one
exports.findOneService = function(req, res, next){
    res.json(req.service);
}; 

//find one update
exports.findOneUpdateService = function(req, res, next){
    Service.findOneAndUpdate({contentOne: req.service.contentOne}, req.body,
        function(err, service){
            if(err){
                return next(err);
            }else{
                res.json(service);
            }
        }
    );
};

//delete
exports.deleteService = function(req, res, next){
    req.service.remove(function(err, service){
        if(err){
            return next(err);
        }else{
            fs.unlinkSync(req.service.path);
            res.json('remove success!');
        }
    });
}

//find id
exports.serviceById = function(req, res, next, _id){
    Service.findOne({
        _id : _id
    }, function(err, service){
        if(err){
            return next(err);
        }else{
            req.service = service;
            next();
        }
    });
};