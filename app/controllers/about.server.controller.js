 var About = require('mongoose').model('about');
 //view all json
 exports.viewAbout = function(req, res, next){
     About.find({}, function(err, about){
         if(err){
             return next(err);
         }else{
             res.json(about);
         }
     });
 };

//findOne About
exports.findOneAbout = function(req, res){
    res.json(req.about);
};

//find one update 
exports.findOneUpdateAbout = function(req, res, next){
    About.findOneAndUpdate({history: req.about.history}, req.body,
        function(err, about){
            if(err){
                return next(err);
            }else{
                res.json(about);
            }
        }
    );
};

//Find id about
exports.aboutById = function(req, res, next, _id){
    About.findOne({
        _id : _id
    }, function(err, about){
        if(err){
            return next(err);
        }else{
            req.about = about;
            next();
        }
    });
};
