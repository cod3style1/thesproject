var Personnel = require('mongoose').model('personnel');
var fs = require('fs');

exports.viewPersonnel = function(req, res, next){
  Personnel.find({}, function(err, personnel){
        if(err){
            return next(err);
        }else{
            res.json(personnel);
        }
    });
}

exports.insertPersonnel = function(req, res, next){
    var path = req.files.image.path;
    var newPersonnel = Personnel();
    newPersonnel.name = req.files.image.name;
    newPersonnel.path = path;
    newPersonnel.save(function(err, personnel){
        if(err){
            res.send('error saving personner');
        }else{
            return res.redirect('/admin');
        }
    });
}

exports.deletePersonnel = function(req, res, next){
    req.personnel.remove(function(err, personnel){
        if(err){
            return next(err);
        }else{
            fs.unlinkSync(req.personnel.path);
            res.json('remove success!');
        }
    });
}

exports.personnelById = function(req, res, next, _id){
    Personnel.findOne({
        _id : _id
    }, function(err, personnel){
        if(err){
            return next(err);
        }else{
            req.personnel = personnel;
            next();
        }
    });
}