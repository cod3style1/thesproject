var Partner = require('mongoose').model('partner');
var fs = require('fs');

exports.viewUpload = function(req, res){
    //res.render('upload');
    Partner.find({}, function(err, partner){
        if(err){
            return next(err);
        }else{
            res.json(partner);
        }
    });
}
//upload file image
exports.uploadFile = function(req, res, next){ 
    var path = req.files.image.path;
    var newPartner = Partner();
    newPartner.name = req.files.image.name;
    newPartner.path = path;
    newPartner.save(function(err, partner){
        if(err){
             res.send('error saving Partner');
        }else{
            return res.redirect('/admin');
        }
    });
}

exports.findOnePartner = function(req, res, path){   
    //decode buffer to base64
    //res.send(new Buffer(req.partner.img.data, 'base64').toString('base64'))

    //console.log(new Buffer("Hello World").toString('base64'))
}

exports.updatePartner = function(req, res, next){
    Partner.findOneAndUpdate({name: req.partner.name},req.body,
        function(err, partner){
            if(err){
                return next(err);
            }else{
                res.json(partner.name);
            } 
        } 
    );
}

exports.deletePartner = function(req, res ,next){
    req.partner.remove(function(err, partner){
        if(err){
            return next(err);
        }else{
            fs.unlinkSync(req.partner.path);
            res.json('remove success!');
        }
    });
}

exports.partnerById = function(req, res, next, _id){
    Partner.findOne({
        _id : _id
    }, function(err, partner){
        if(err){
            return next(err);
        }else{
            req.partner = partner;
            next();
        }
    });
}