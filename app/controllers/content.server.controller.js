exports.Test = function(req, res){
   res.render('index',{
       username: req.user ? req.user.username: ''
   });
};

var Content = require('mongoose').model('content');

exports.create = function(req,res, next) { 
    var content = new Content(req.body);
    content.titleContent = req.body.titleContent;
    content.textContent = req.body.textContent;
    content.save(function(err){
        if(err){
            return next(err);
        }else{
            res.json(content);
        }
    });
};

exports.newContent = function(req, res){
    var NewContent = new Content();
    NewContent.titleContent = req.body.titleContent;
    NewContent.textContent = req.body.textContent;
    NewContent.save(function(err, content){
        if(err){
            res.send('error saving content');
        }else{
            console.log(content);
            res.send(content);
        }
    });
};

exports.renderCreate = function(req, res){
    res.render('createContent');
};

exports.renderBackEnd = function(req, res){
    if(!req.user){
        res.render('login');
    }else{
       return res.render('backEnd');
    }
};

exports.listContent = function(req, res, next){ 
    Content.find({}, function(err, content){
        if(err){
            return next(err);
        }else{
            res.json(content);
        }
    });
};

exports.renderContents = function(req, res){
    res.render('viewContents');
};

exports.update = function(req, res){
    Content.findOne({ _id : req.params._id}, function(err, content){
        if(err){
            return res.send(err);
        }

        for(prop in req.body){
            content[prop] = req.body[prop];
        }

        content.save(function(err){
            if(err){
                return res.send(err)
            }
            res.json({massage: 'content update!!'});
        });
    });
};

exports.delete = function(req, res){
    Content.remove({
        _id : req.params._id
    }, function(err, content){
        if(err){
            return res.send(err);
        }
        res.json({message: 'deleted!!'});
    });
}

