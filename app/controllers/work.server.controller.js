var Work = require('mongoose').model('work');
var fs = require('fs');
//view all
exports.viewWork = function(req, res, next){
    Work.find({}, function(err, work){
        if(err){
            return next(err);
        }else{
            res.json(work);
        }
    });
}

//insert work
exports.insertWork = function(req, res, next){
    var path = req.files.image.path;
    var newWork = new Work();
    newWork.topic = req.body.topic; 
    newWork.content = req.body.content;
    newWork.name = req.files.image.name;
    newWork.path = path;
    newWork.save(function(err, work){
        if(err){
            return next(err);
        }else{
            return res.redirect('/admin');
        }
    });
};

//find one
exports.findOneWork = function(req, res){
    res.json(req.work);
}

//find one delete 
exports.findOneDeleteWork = function(req, res,  next){
    req.work.remove(function(err, work){
        if(err){
            return next(err);
        }else{
            //fs.unlinkSync(req.work.path);
            res.json('remove success!');
        }
    });
};



//find one update
exports.findOneUpdateWork = function(req, res, next){
    Work.findOneAndUpdate({topic: req.work.topic}, req.body,
        function(err, work){
            if(err){
                return next(err); 
            }else{
                res.json(work);
            }
        }
    );
}

//find id
exports.workById = function(req, res, next, _id){
    Work.findOne({
        _id : _id
    }, function(err, work){
        if(err){
            return next(err);
        }else{
            req.work = work;
            next()
        }
    });
}