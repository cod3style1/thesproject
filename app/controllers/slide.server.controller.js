var Slide = require('mongoose').model('slide');
var fs = require('fs');
exports.viewSlide = function(req, res, next){
    //res.render('upload');
  Slide.find({}, function(err, slide){
        if(err){
            return next(err);
        }else{
            res.json(slide);
        }
    });
}

exports.insertSlide = function(req, res, next){
    var path = req.files.image.path;
    var newSlide = Slide();
    newSlide.name = req.files.image.name;
    newSlide.path = path;
    newSlide.save(function(err, slide){
        if(err){
            res.send('error saving slide');
        }else{
            return res.redirect('/admin');
        }
    });
}

exports.deleteSlide = function(req, res, next){
    req.slide.remove(function(err, slide){
        if(err){
            return next(err);
        }else{
            fs.unlinkSync(req.slide.path);
            res.json('remove success!');
        }
    });
}


exports.slideById = function(req, res, next, _id){
    Slide.findOne({
        _id : _id
    }, function(err, slide){
        if(err){
            return next(err);
        }else{
            req.slide = slide;
            next()
        }
    });
}
