var express = require('express');
var fs = require('fs');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var sass  = require('node-sass-middleware');
var validator = require('express-validator');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var methodOverride = require('method-override'); 
var RedisStore = require('connect-redis')(session);
var config = require('./config');
var multer = require('multer');


module.exports = function(){
    var app = express();

    if(process.env.NODE_ENV === 'development'){
        app.use(morgan('dev'));
    }else{
        app.use('compression');
    }
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(validator());

    app.use(session({
        secret: config.sessionSecret,
        resave: false,
        saveUninitialized: true
    }));
    app.use(session({
        store: new RedisStore({
            host: 'localhost',
            port: 6379,
            db: 2,
            pass: 'redis_password'
        }),
        secret: 'secret_key'
    }));
    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(multer({
        dest: './public/images',
        rename: function(fieldname, filename) {
            return Date.now();
        },
        limits: {
            fileSize: 100000000
        },
        onFileSizeLimit: function(file) {
            console.log('Failed: ' + file.originalname + ' is limited');
            fs.unlink(file.path);
        }
    }));
    app.set('views',['./app/views','./public']);
    app.set('view engine','jade','html');

    require('../app/routes/index.server.routes')(app);
    require('../app/routes/user.server.routes')(app);
    require('../app/routes/partial.server.routes')(app);
    require('../app/routes/content.server.routes')(app);
    require('../app/routes/contact.server.routes.js')(app);
    require('../app/routes/about.server.routes.js')(app);
    require('../app/routes/service.server.routes.js')(app);
    require('../app/routes/work.server.routes.js')(app);
    require('../app/routes/partner.server.routes.js')(app);
    require('../app/routes/personnel.server.routes.js')(app);
    require('../app/routes/slide.server.routes.js')(app);
    app.use(sass({
        src:'./sass',
        dest:'./public/css',
        outputStyle:'compressed',
        prefix: '/css',
        debug: true
    }))
    app.use(express.static('./public'));

    return app;
}