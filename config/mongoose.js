var config = require('./config');
var mongoose = require('mongoose');

//ติดต่อ db
module.exports = function(){
    mongoose.set('debug', config.debug);
    var db = mongoose.connect(config.mongoUri);
    require('../app/models/user.server.model');
    require('../app/models/content.server.model');
    require('../app/models/contact.server.model');
    require('../app/models/about.server.model');
    require('../app/models/service.server.model');
    require('../app/models/work.server.model');
    require('../app/models/partner.server.model');
    require('../app/models/personnel.server.model');
    require('../app/models/slide.server.model');
    return db;
};